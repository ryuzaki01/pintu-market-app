import React from "react";
import { StatusBar } from 'expo-status-bar';
import { AppStateStatus, Platform } from 'react-native';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { QueryClient, QueryClientProvider, focusManager } from 'react-query';

import useCachedResources from './hooks/useCachedResources';
import useColorScheme from './hooks/useColorScheme';
import Navigation from './navigation';
import useOnlineManager from "./hooks/useOnlineManager";
import useAppState from "./hooks/useAppState";

function onAppStateChange(status: AppStateStatus) {
  // React Query already supports in web browser refetch on window focus by default
  if (Platform.OS !== 'web') {
    focusManager.setFocused(status === 'active');
  }
}

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      queryFn: (request) => {
        const { queryKey } = request;

        return fetch(`https://api.pintu.co.id/v2${queryKey[0]}`).then(res => res.json()).catch(e => ({
          code: 'error',
          message: e.message,
          payload: []
        }));
      },
      retry: 2
    },
  },
});

export default function App() {
  useOnlineManager();
  useAppState(onAppStateChange);
  const isLoadingComplete = useCachedResources();
  const colorScheme = useColorScheme();

  if (!isLoadingComplete) {
    return null;
  } else {
    return (
      <SafeAreaProvider>
        <QueryClientProvider client={queryClient}>
          <Navigation colorScheme={colorScheme} />
        </QueryClientProvider>
        <StatusBar />
      </SafeAreaProvider>
    );
  }
}
