import { useEffect } from 'react';
import { AppState, AppStateStatus } from 'react-native';

const useAppState = (onChange: (status: AppStateStatus) => void) => {
  useEffect(() => {
    const changeListener = AppState.addEventListener('change', onChange);

    return () => {
      changeListener.remove();
    };
  }, [onChange]);
}

export default useAppState;