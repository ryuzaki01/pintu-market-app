import React, { useState } from "react";
import { StyleSheet, FlatList } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { useQueries } from 'react-query';
import SelectDropdown from 'react-native-select-dropdown'

import {Text, View, TouchableOpacity, useThemeColor} from '../components/Themed';
import Tags, { TagType } from '../constants/Tags';
import {Currency, Price, PriceChangeResponse, SupportedCurrencyResponse} from "../types";
import CurrencyItem from "../components/CurrencyItem";
import Colors from "../constants/Colors";

export type CurrencySort = 'default' | 'gainer' | 'loser' | 'name-desc' | 'name-asc';
export type CurrencyCombine = Currency & Price;

const sortData = (sort: CurrencySort, data: Currency[], prices: Price[]) => {
  const [_, ...restData] = data;
  const combinedData = restData.map((d, i) => ({
    ...d,
    ...prices[i]
  })) as CurrencyCombine[];

  if (sort === 'gainer') {
    return combinedData.sort((a, b) => {
      if ( b.day < a.day ){
        return -1;
      }
      if ( b.day > a.day ){
        return 1;
      }
      return 0;
    })
  }

  if (sort === 'loser') {
    return combinedData.sort((a, b) => {
      if ( a.day < b.day ){
        return -1;
      }
      if ( a.day > b.day ){
        return 1;
      }
      return 0;
    })
  }

  if (sort === 'name-desc') {
    return combinedData.sort((a, b) => {
      if ( a.currencySymbol < b.currencySymbol ){
        return -1;
      }
      if ( a.currencySymbol > b.currencySymbol ){
        return 1;
      }
      return 0;
    })
  }

  if (sort === 'name-asc') {
    return combinedData.sort((a, b) => {
      if ( b.currencySymbol < a.currencySymbol ){
        return -1;
      }
      if ( b.currencySymbol > a.currencySymbol ){
        return 1;
      }
      return 0;
    })
  }

  return combinedData;
};

const SORT_SELECTIONS = [
  { label: 'Default', value: 'default' },
  { label: 'Gainer (24 Hours)', value: 'gainer' },
  { label: 'Loser (24 Hours)', value: 'loser' },
  { label: 'Asset Name (A-Z)', value: 'name-desc' },
  { label: 'Asset Name (Z-A)', value: 'name-asc' },
];

export default function MarketScreen() {
  const color = useThemeColor({ light: Colors.light.text, dark: Colors.dark.text }, 'text');
  const [currencyResponse, priceResponse] = useQueries([
    {
      queryKey: '/wallet/supportedCurrencies'
    },
    {
      queryKey: '/trade/price-changes',
      refetchInterval: 2000,
    }
  ]);

  const [sort, setSort] = useState<CurrencySort>('default');

  return (
    <View style={styles.container}>
      <FlatList
        horizontal
        style={styles.tagContainer}
        data={Object.keys(Tags)}
        keyExtractor={(item) => item}
        showsHorizontalScrollIndicator={false}
        renderItem={({ item: tag }) => {
          const { title, icon } = Tags[tag] as TagType;

          return (
            <TouchableOpacity key={`tag-${tag}`} style={styles.tag}>
              <View style={{ flexDirection: 'row' }}>
                <Ionicons name={icon as any} size={15} color="#fff"/>
                <Text style={styles.tagTitle}>{title}</Text>
              </View>
            </TouchableOpacity>
          )
        }}
      />
      <View style={styles.sortContainer}>
        <View>
          <Text>Sort By</Text>
        </View>
        <SelectDropdown
          defaultValue={sort}
          onSelect={a => setSort(a.value)}
          data={SORT_SELECTIONS}
          renderDropdownIcon={(a) => (<Ionicons name="chevron-down" size={20} color={color} />)}
          buttonStyle={{
            height: 30,
            backgroundColor: 'transparent'
          }}
          buttonTextStyle={{
            color: color,
            fontSize: 12
          }}
          rowTextStyle={{
            margin: 0,
            padding: 0
          }}
          buttonTextAfterSelection={(item) => {
            // text represented for each item in dropdown
            // if data array is an array of objects then return item.property to represent item in dropdown
            return item.label
          }}
          rowTextForSelection={(item) => {
            // text represented for each item in dropdown
            // if data array is an array of objects then return item.property to represent item in dropdown
            return item.label
          }}
        />
      </View>
      <FlatList
        data={sortData(sort,(currencyResponse?.data as SupportedCurrencyResponse)?.payload || [], (priceResponse?.data as PriceChangeResponse)?.payload || [])}
        keyExtractor={(item) => item.currencySymbol}
        renderItem={({ index, item: currency}) => {
          return (
            <CurrencyItem
              key={`currency-${currency.currencySymbol}`}
              data={currency}
            />
          )
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  tagContainer: {
    borderWidth: 1,
    borderBottomWidth: 2,
    paddingHorizontal: 10,
    height: 50,
    flexDirection: 'row',
    marginVertical: 10,
    marginHorizontal: 5,
  },
  tag: {
    borderWidth: 1,
    borderRadius: 20,
    borderColor: '#FFF',
    color: '#888',
    marginLeft: 5,
    paddingHorizontal: 10,
    paddingVertical: 10
  },
  tagTitle: {
    marginLeft: 2
  },
  sortContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    paddingBottom: 10
  }
});
