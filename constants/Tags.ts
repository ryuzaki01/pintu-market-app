export default {
  "new": {
    title: "New",
    icon: "md-star-outline"
  },
  "defi": {
    title: "Defi",
    icon: "ios-logo-apple-appstore"
  },
  "gaming": {
    title: "NFT/Gaming",
    icon: "game-controller-outline"
  },
  "cex": {
    title: "CEX",
    icon: "trending-up-outline"
  },
  "dex": {
    title: "DEX",
    icon: "repeat-sharp"
  },
  "layer1": {
    title: "Layer-1",
    icon: "layers-outline"
  },
  "infra": {
    title: "Infrastructure",
    icon: "server-outline"
  },
  "lending": {
    title: "Lending",
    icon: "cash-outline"
  },
  "layer2": {
    title: "Layer-2",
    icon: "layers"
  },
  "stableCoin": {
    title: "Stablecoin Ecosystem",
    icon: "ios-library-sharp"
  }
} as TagsType

export interface TagType {
  title: string;
  icon: string;
}

export type TagsType = Record<string, TagType>;