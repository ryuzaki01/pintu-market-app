import React from 'react';
import { SvgUri } from 'react-native-svg';
import AnimatedText from "./AnimatedText";
import {Currency, Price} from "../types";
import {Text, View} from "./Themed";
import { StyleSheet } from "react-native";

export interface CurrencyItemProps {
  data: Currency & Price;
}

export default function CurrencyItem(props: CurrencyItemProps) {
  const { data } = props;
  const {
    currencySymbol,
    name,
    logo,
    color,
    latestPrice,
    day,
  } = data || {};

  return (
    <View style={styles.container}>
      <SvgUri uri={logo} fill={color} height={40} width={40} color={color} style={styles.symbol} />
      <View style={styles.nameContainer}>
        <Text style={{ fontWeight: 'bold' }}>{name}</Text>
        <Text>{currencySymbol}</Text>
      </View>
      <View style={styles.priceContainer}>
        <AnimatedText fade type="currency" value={latestPrice} />
        <AnimatedText type="percentage" value={day} />
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'stretch',
    justifyContent: 'space-between',
    paddingVertical: 10,
    paddingHorizontal: 10,
    borderBottomColor: '#fff',
    borderBottomWidth: 1
  },
  symbol: {
    marginRight: 10
  },
  nameContainer: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'flex-start'
  },
  priceContainer: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'flex-end'
  }
});