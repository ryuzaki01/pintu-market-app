import React, { useEffect, useRef, useState } from 'react';
import { Animated } from 'react-native';
import { currency } from "../utils";
import CompositeAnimation = Animated.CompositeAnimation;
import {useThemeColor} from "./Themed";
import Colors from "../constants/Colors";

export type TextType = 'currency' | 'percentage';

export interface AnimatedTextProps {
  fade?: boolean,
  type: TextType
  value: string
}

const getText = (value: string, type: TextType) => {
  if (type === 'currency') {
    return currency('Rp', value);
  }

  const parsedValue = parseFloat(value) || 0.00;

  return `${parsedValue > 0 ? '+' : ''}${parsedValue.toFixed(2)}%`
}

export default function AnimatedText(props: AnimatedTextProps) {
  const { value: defaultValue, type, fade } = props;
  const color = useThemeColor({ light: Colors.light.text, dark: Colors.dark.text }, 'text');
  const parsedValue = parseFloat(defaultValue);
  const [value, setValue] = useState(parsedValue);
  const textColorRef = useRef(new Animated.Value(0));
  const textColor = textColorRef.current.interpolate({
    inputRange: [-1, 0, 1],
    outputRange: ['#fb4e4e', color, '#54f854']
  })

  useEffect(() => {
    const newValue = parseFloat(defaultValue);
    const diff = newValue - value;

    const colorDiff = type === 'currency' ? diff : newValue;

    Animated.sequence([
      colorDiff > 0.00 && Animated.timing(textColorRef.current, {
        toValue: 1,
        duration: 500,
        useNativeDriver: false
      }),
      colorDiff < 0.00 && Animated.timing(textColorRef.current, {
        toValue: -1,
        duration: 500,
        useNativeDriver: false
      }),
      fade && Animated.timing(textColorRef.current, {
        toValue: 0,
        duration: 500,
        delay: 1000,
        useNativeDriver: false
      })
    ].filter(Boolean) as CompositeAnimation[]).start();

    setValue(newValue);
  }, [defaultValue]);

  return <Animated.Text style={{ color: textColor }}>{getText(defaultValue, type)}</Animated.Text>
};
